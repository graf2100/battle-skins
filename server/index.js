const express = require('express');
let app = express();
const http = require('http').Server(app);
const io = require('socket.io')(http);
const path = require('path');

const db = require('./dataBase');
const Models = require('./dataBase/models');

app.use(express.static(path.join(__dirname, '..', '/public')));

app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname, '..', '/public', 'index.html'));
});

let testUset = new Models.users({
    _id: Math.random().toString(),
    googleID: '1',
    displayName: '1',
    avatar: '1',
    email: '1',
    balance: 0,
});

testUset.save(function (err, user) {
    if (err) return console.error(err);
    console.log(user);
});

io.on('connection', socket => {

    socket.on('user', id => {
        users.find({id}, date => {
            socket.emit('user', date);
        });
    });

    socket.on('disconnect', () => {
        console.log('user disconnected');
    });
});

http.listen(process.env.PORT || 8080, () => {
    console.log('Server started)');
});


