const db = require('mongoose');

const userSchema = new db.Schema({
    _id: {type: String},
    googleID: {type: String},
    displayName: {type: String},
    avatar: {type: String},
    email: {type: String},
    balance: {type: Number, default: 0, min: 0},
    muted: {type: Boolean, default: true},
    mutedToDate: {type: Date, default: new Date()},
    disabledDeposit: {type: Boolean, default: false},
    disabledDepositToDate: {type: Date, default: 0},
    disabledWithdrawal: {type: Boolean, default: false},
    disabledWithdrawalToDate: {type: Date, default: 0},
}, {
    timestamps: true
});


userSchema.methods.getPublicFields = function () {
    return {
        id: this._id,
        displayName: this.displayName,
        avatar: this.avatar,
        balance: this.balance,
        muted: this.muted,
        disabledDeposit: this.disabledDeposit,
        disabledWithdrawal: this.disabledWithdrawal
    }
};

module.exports = db.model('users', userSchema);
