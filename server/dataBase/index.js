const db = require('mongoose');
const config = require('../config');

db.Promise = Promise;
db.connect(config['MONGOOSE_URI'], {useNewUrlParser: true}, (error) => {
    if (error) {
        console.error(error);
        process.exit(1);
    }
    console.log('MONGO CONNECTED: ' + config['MONGOOSE_URI']);
});
module.export = db;